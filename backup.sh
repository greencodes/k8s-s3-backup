#!/bin/bash

ROOT="/ceph"                        # Root directory of the folders to back up
DEST="satellite:satellite-imaging"  # target S3 bucket to backup to

for f in `cat $SCRIPTLOC/backup-list.txt`; do rclone copy --no-traverse -P "$ROOT/$f" "$DEST/$f"; done
