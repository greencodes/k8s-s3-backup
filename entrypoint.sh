#!/bin/bash

SCRIPTLOC="/ceph/k8s-s3-backup"

# persist SCRIPTLOC
echo "export SCRIPTLOC=$SCRIPTLOC" >> /etc/bash.bashrc

# install dependencies
apt update && apt install -y man cron
curl https://rclone.org/install.sh | bash

# setup rclone
cp "$SCRIPTLOC/rclone.conf" /root/.config/rclone/

# write crontab and start cron service 
# NOTE: time in UTC, 12:00pm UTC = 04:00am PST = 05:00am PDT
echo "00  10    * * *   root    bash $SCRIPTLOC/backup.sh" >> /etc/crontab
service cron start

# enter sleep
sleep infinity
